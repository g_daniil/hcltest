package daniil.goriunov.HCL.Controller;

import daniil.goriunov.HCL.Exception.ResourceNotFoundException;
import daniil.goriunov.HCL.Model.Project;
import daniil.goriunov.HCL.Repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class ProjectController {

  @Autowired
  private ProjectRepository projectRepository;

  @GetMapping("/projects")
  public List<Project> getAllProjects() {
    return projectRepository.findAll();
  }

  @GetMapping("/projects/{id}")
  public ResponseEntity<Project> getProjectById(@PathVariable(value = "id") Long projectId)
      throws ResourceNotFoundException {
	  Project project =
        projectRepository
            .findById(projectId)
            .orElseThrow(() -> new ResourceNotFoundException("Project not found " + projectId));
    return ResponseEntity.ok().body(project);
  }
  
  @PostMapping("/projects")
  public Project createProject(@Valid @RequestBody Project project) {
    return projectRepository.save(project);
  }

  @PutMapping("/project/{id}")
  public ResponseEntity<Project> updateProject(
      @PathVariable(value = "id") Long projectId, @Valid @RequestBody Project projectDetails)
      throws ResourceNotFoundException {

	Project project =
        projectRepository
            .findById(projectId)
            .orElseThrow(() -> new ResourceNotFoundException("Project not found " + projectId));

	project.setStartDate(projectDetails.getStartDate());
	project.setEndDate(projectDetails.getEndDate());
	project.setName(projectDetails.getName());
    final Project updatedProject = projectRepository.save(project);
    return ResponseEntity.ok(updatedProject);
  }

  @DeleteMapping("/project/{id}")
  public Map<String, Boolean> deleteProject(@PathVariable(value = "id") Long projectId) throws Exception {
    Project project =
        projectRepository
            .findById(projectId)
            .orElseThrow(() -> new ResourceNotFoundException("Project not found " + projectId));

    projectRepository.delete(project);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return response;
  }
}
