package daniil.goriunov.HCL;

import java.sql.Date;

import daniil.goriunov.HCL.Model.Project;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HclApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HclApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetAllProjects() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/projects",
				HttpMethod.GET, entity, String.class);

		Assert.assertNotNull(response.getBody());
	}

	@Test
	public void testGetProjectById() {
		Project project = restTemplate.getForObject(getRootUrl() + "/projects/1", Project.class);
		System.out.println(project.getName());
		Assert.assertNotNull(project);
	}

	@Test
	public void testCreateProject() {
		Project project = new Project();
		project.setName("Walls");
		Date now = new Date(new java.util.Date().getTime());
		project.setStartDate(now);
		project.setEndDate(now);

		ResponseEntity<Project> postResponse = restTemplate.postForEntity(getRootUrl() + "/projects", project, Project.class);
		Assert.assertNotNull(postResponse);
		Assert.assertNotNull(postResponse.getBody());
	}

	@Test
	public void testUpdatePost() {
		int id = 1;
		Project project = restTemplate.getForObject(getRootUrl() + "/projects/" + id, Project.class);
		project.setName("Foundation");

		restTemplate.put(getRootUrl() + "/projects/" + id, project);

		Project updatedProject = restTemplate.getForObject(getRootUrl() + "/projects/" + id, Project.class);
		Assert.assertNotNull(updatedProject);
	}

	@Test
	public void testDeletePost() {
		int id = 2;
		Project project = restTemplate.getForObject(getRootUrl() + "/projects/" + id, Project.class);
		Assert.assertNotNull(project);

		restTemplate.delete(getRootUrl() + "/projects/" + id);

		try {
			project = restTemplate.getForObject(getRootUrl() + "/projects/" + id, Project.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

}